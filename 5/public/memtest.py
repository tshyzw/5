#!/usr/bin/python
import sys
if len(sys.argv) == 2:
   try:
     n = int(sys.argv[1])
   except ValueError:
     n = 10000
   if n > 9999:
      print "number range 1 ~ 9999"
   else:
      memlist, memstr = [], ' ' * 1024 * 1024
      memlist.append(memstr * n)
      _ = raw_input('use memory success\npress any key to exit : ')
else:
   print "%s number (MB)" % sys.argv[0]
